const gulp = require('gulp');
const ts = require('gulp-typescript');
const tsModuleBundler = require('gulp-typescript-module-bundler');
const browserify = require('gulp-browserify');
const spawn = require('child_process').spawn;
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const pump = require('pump');
var node;

gulp.task('scripts', () => {
  const tsProject = ts.createProject('tsconfig.json');
  const tsResult = tsProject.src()
    .pipe(sourcemaps.init())
    .pipe(tsProject());
  return tsResult.js
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/tmp'))
});

gulp.task('server', ['scripts'], function() {
  if (node) node.kill()
  node = spawn('node', ['dist/tmp/index.js'], {stdio: 'inherit'})
  node.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
})

gulp.task('watch', ['server'], () => {
  gulp.watch('src/**/*.ts', ['lint', 'server']);
});

gulp.task('default', ['watch']);

// clean up if an error goes unhandled.
process.on('exit', function() {
  if (node) node.kill()
});
