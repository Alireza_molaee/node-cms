import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as logger from 'morgan';
import * as path from 'path';

// Creates and configures an ExpressJS web server.
class App {

  public express: express.Application;

  constructor() {
    this.express = express();
    this.setup();
  }

  private setup(): void {
    this.express.use(logger('dev'));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use('/', site);
  }

}

export default new App().express;
