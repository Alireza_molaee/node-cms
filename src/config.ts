interface AppConfig {
    port: number|string|boolean;
}

function initConfig(): AppConfig {
    const c: AppConfig = {
        port: normalizePort(3000),
    };
    return c;
}

function normalizePort(val: number|string): number|string|boolean {
  const iport: number = (typeof val === 'string') ? parseInt(val, 10) : val;
  if (isNaN(iport)) {
    return val;
  } else if (iport >= 0) {
    return iport;
  } else {
    return false;
  }
}

const config: AppConfig = initConfig();

export default config;
